# Dokumentation zur Solar Wetterstation

## Links und Dokumente im Web

Ein interessanten Einstieg in des Thema gibt es bei [Wettermonster](https://wettermonster.de/) 

## Bezugsquellen 

+ Sparkfun [Aruino Weather Shield](https://learn.sparkfun.com/tutorials/arduino-weather-shield-hookup-guide-v12) eine Anleitung mit Code Beispielen
+ Renkforce AOK-5055 Funk-Wetterstation bei [Conrad](https://www.conrad.de/de/renkforce-aok-5055-funk-wetterstation-vorhersage-fuer-12-bis-24-stunden-1267773.html)


